from pickle import GET

from flask import Flask, jsonify
from flask import request
import pyodbc

app = Flask(__name__)

server = 'tcp:35.205.113.17'
database = 'rest'
username = 'sqlserver'
password = 'VeryStrongPass!'
cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
cursor = cnxn.cursor()

def sql_win_amount(member_id, ym = 'all time', game_id = 'all games'):
    sql = 'select ROUND(SUM([WIN_AMOUNT]),2) r from [dbo].[REVENUE_ANALYSIS] ' \
              'where [MEMBER_ID]={member_id}'.format(member_id=member_id)
    if ym != 'all time':
        sql = sql + 'and [ACTIVITY_YEAR_MONTH]={ym}'.format(ym = ym)

    if game_id != 'all games':
        sql = sql + 'and [GAME_ID]={game_id}'.format(game_id = game_id)

    cursor.execute(sql)
    for row in cursor:
        answer = row.r
    return answer

def sql_wager_amount(member_id, ym = 'all time', game_id = 'all games'):
    sql = 'select ROUND(SUM([WAGER_AMOUNT]),2) r ' \
          'from [dbo].[REVENUE_ANALYSIS] ' \
          'where [MEMBER_ID]={member_id}'.format(member_id=member_id)

    if ym != 'all time':
        sql = sql + 'and [ACTIVITY_YEAR_MONTH]={ym}'.format(ym = ym)

    if game_id != 'all games':
        sql = sql + 'and [GAME_ID]={game_id}'.format(game_id = game_id)

    cursor.execute(sql)
    for row in cursor:
        answer = row.r
    return answer

def sql_wagers_count(member_id, ym = 'all time', game_id = 'all games'):
    sql = 'select ROUND(count([MEMBER_ID]),2) r ' \
          'from [dbo].[REVENUE_ANALYSIS] ' \
          'where [MEMBER_ID]={member_id}'.format(member_id=member_id)

    if ym != 'all time':
        sql = sql + 'and [ACTIVITY_YEAR_MONTH]={ym}'.format(ym = ym)

    if game_id != 'all games':
        sql = sql + 'and [GAME_ID]={game_id}'.format(game_id = game_id)

    cursor.execute(sql)
    for row in cursor:
        answer = row.r
    return answer

@app.route('/api/win_amount/<int:member_id>', methods=['GET'])
def win_amount(member_id):
    ym='all time'
    game_id = 'all games'
    if 'ym' in request.args.keys(): ym=request.args.get('ym')
    if 'game_id' in request.args.keys(): game_id = request.args.get('game_id')

    return jsonify({'member_id': member_id,
                    'operation': "win amount",
                    "time": ym,
                    "game_id": game_id,
                    "result": sql_win_amount(member_id = member_id, ym = ym, game_id = game_id)})


@app.route('/api/wager_amount/<int:member_id>', methods=['GET'])
def wager_amount(member_id):
    ym = 'all time'
    game_id = 'all games'
    if 'ym' in request.args.keys(): ym = request.args.get('ym')
    if 'game_id' in request.args.keys(): game_id = request.args.get('game_id')

    return jsonify({'member_id': member_id,
                    'operation': "wager amount",
                    "time": ym,
                    "game_id": game_id,
                    "result": sql_wager_amount(member_id=member_id, ym=ym, game_id=game_id)})

@app.route('/api/wagers_count/<int:member_id>', methods=['GET'])
def wagers_count(member_id):
    ym = 'all time'
    game_id = 'all games'
    if 'ym' in request.args.keys(): ym = request.args.get('ym')
    if 'game_id' in request.args.keys(): game_id = request.args.get('game_id')

    return jsonify({'member_id': member_id,
                    'operation': "wager count",
                    "time": ym,
                    "game_id": game_id,
                    "result": sql_wagers_count(member_id=member_id, ym=ym, game_id=game_id)})

@app.route('/api/help/', methods=['GET'])
def help():


    return jsonify([
                {"Win amount": "/api/win_amount/<int:member_id>?game_id=<int, optional>&ym=<yyyymm, optional>"},
                {"Wager amount": "/api/wager_amount/<int:member_id>?game_id=<int, optional>&ym=<yyyymm, optional>"},
                {"Wagers count": "/api/wagers_count/<int:member_id>?game_id=<int, optional>&ym=<yyyymm, optional>"}
                ])


app.run('127.0.0.1', port=80, debug=True)